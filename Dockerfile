FROM ubuntu:latest
WORKDIR /rumbl


RUN apt-get clean && apt-get update && apt-get install -y postgresql-client locales wget gnupg2 apt-utils
# Set the locale to UTF8 because elixir absolutly need UTF-8
RUN locale-gen en_US.UTF-8 && locale-gen en en_US en_US.UTF-8
RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen &&  locale-gen
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

# install Elixir and it's dependencies
RUN wget https://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb && dpkg -i erlang-solutions_1.0_all.deb
RUN apt-get update && apt-get install -y esl-erlang elixir nodejs npm inotify-tools git-core

# install node packages (phoenix uses brunch)
# WORKDIR /rumbl/assets
# RUN ls -al
# RUN npm install
# WORKDIR /rumbl
# RUN ls -al
# install hex, the elixir package manager and rebar, the erlang build tool
# we use --force to avoid having to confirm the install
RUN mix local.hex --force
RUN mix local.rebar --force

# install the Phoenix framework, it's a web framework for Elixir
RUN mix archive.install https://github.com/phoenixframework/archives/raw/master/phx_new.ez --force

# run the server
CMD ["/rumbl/start.sh"]
