To launch the docker, simply use docker-compose up, then, when it's ready, access http://localhost:4000.

### Application

This docker run the Rumbl application, which is the example application that is created during the different exercises of the Programming Phoenix 1.4 book.

### Architecture

This Docker exercise include 2 dockers : one for the app, and the other for Postgresql, the database system used by the app.

The app docker, named Phoenix, use the ubuntu image.

The database docker, named postres, use the postres image and set a volume to be able to exchange data with the phoenix image.

### Startup

After the images are built and ready to run, we first start the postgres docker, which will configure the database if it isn't configured. Then, when it's ready, the phoenix docker starts and run the start.sh script, which will wait for the postgresql database to be ready for use, the database will be created if it doesn't exist already and then, Phoenix starts the application.

You can check the variable used for the database, like the username, in the docker-compose.yml file.

### Production
To switch into production, simply change the MIX_ENV variable in docker-compose.yml from dev to prod, Phoenix will then build the project with the config/prod.exs config file.

You can simply see if it worked by going in a non existant page, the error will be much less detailed. Also, the log that is displayed in the command prompt only list the requests, but not all the details of the framework.

### links

[Elixir](https://elixir-lang.org/)

[Phoenix](https://phoenixframework.org/)

[Programming Phoenix 1.4](https://pragprog.com/book/phoenix14/programming-phoenix-1-4)
